<?php
/* @var $this EquipmentController */
/* @var $model Equipment */

$this->breadcrumbs=array(
	'Equipments'=>array('index'),
	$model->id,
);

$this->menu=array(
	//array('label'=>'List Equipment', 'url'=>array('index')),
	array('label'=>'Create Equipment', 'url'=>array('create')),
	array('label'=>'Update Equipment', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Equipment', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Equipment', 'url'=>array('admin')),
);
/*$this->widget(
    'application.modules.auditTrail.widgets.portlets.ShowAuditTrail',
    array(
        'model' => $model,
    )
);*/
?>

<h1>View Equipment #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'company_code',
		'category',
		'description',
		'quantity',
		'imp',
		'si',
		's_n_chassis',
		's_n_engine',
		'plate_number',
		'year',
		'location',
		'maintenance_status',
		'last_service_date',
		'last_service_hour_km',
		'last_recording_hrs_km',
		'delta_km_hr',
		'status',
		'speedometer',
		'from',
		'origin_docs',
		'customs_declaration',
		'im_status',
		'custom_declaration_export',
		'note',
		'im_40',
		'danger_perm',
		'temp_registr',
		'tech_from',
		'till',
		'sanitary',
	),
)); 
	

?>