<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
 /* this is original
class UserIdentity extends CUserIdentity
{
	public function authenticate()
	{
		$users=array(
			// username => password
			'demo'=>'demo',
			'admin'=>'admin',
		);
		if(!isset($users[$this->username]))
			$this->errorCode=self::ERROR_USERNAME_INVALID;
		elseif($users[$this->username]!==$this->password)
			$this->errorCode=self::ERROR_PASSWORD_INVALID;
		else
			$this->errorCode=self::ERROR_NONE;
		return !$this->errorCode;
	}
}*/
//end original
class UserIdentity extends CUserIdentity
{
   private $_id;
   public function authenticate()
   {
       $record=Employee::model()->findByAttributes(array('e_email'=>$this->username));  // here I use Email as user name which comes from database
       if($record===null)
               {
                       $this->_id='user Null';
                                   $this->errorCode=self::ERROR_USERNAME_INVALID;
               }
       else if($record['e_password']!==$this->password)            // here I compare db password with passwod field
               {        $this->_id=$this->username;
                        $this->errorCode=self::ERROR_PASSWORD_INVALID;
               }
 /*        else if($record['e_status']!=='Active')                //  here I check status as Active in db
               {        
                                        $err = "You have been Inactive by Admin.";
                                $this->errorCode = $err;
               }
   */     
       else
       {  
          $this->_id=$record['e_name'];
           $this->setState('title', $record['e_name']);
           $this->errorCode=self::ERROR_NONE;

       }
       return !$this->errorCode;
   }

   public function getId()       //  override Id
   {
       return $this->_id;
   }
}