<?php
/* @var $this LaborerController */
/* @var $model Laborer */

$this->breadcrumbs=array(
	'Laborers'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Laborer', 'url'=>array('index')),
	array('label'=>'Create Laborer', 'url'=>array('create')),
	array('label'=>'View Laborer', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Laborer', 'url'=>array('admin')),
);
?>

<h1>Update Laborer <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>