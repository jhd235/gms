<?php
/* @var $this EquipmentController */
/* @var $data Equipment */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plate_number')); ?>:</b>
	<?php echo CHtml::encode($data->plate_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('category')); ?>:</b>
	<?php echo CHtml::encode($data->category); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('description')); ?>:</b>
	<?php echo CHtml::encode($data->description); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('quantity')); ?>:</b>
	<?php echo CHtml::encode($data->quantity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('imp')); ?>:</b>
	<?php echo CHtml::encode($data->imp); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('si')); ?>:</b>
	<?php echo CHtml::encode($data->si); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('s_n_chassis')); ?>:</b>
	<?php echo CHtml::encode($data->s_n_chassis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('s_n_engine')); ?>:</b>
	<?php echo CHtml::encode($data->s_n_engine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('plate_number')); ?>:</b>
	<?php echo CHtml::encode($data->plate_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('location')); ?>:</b>
	<?php echo CHtml::encode($data->location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('maintenance_status')); ?>:</b>
	<?php echo CHtml::encode($data->maintenance_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_service_date')); ?>:</b>
	<?php echo CHtml::encode($data->last_service_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_service_hour_km')); ?>:</b>
	<?php echo CHtml::encode($data->last_service_hour_km); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('last_recording_hrs_km')); ?>:</b>
	<?php echo CHtml::encode($data->last_recording_hrs_km); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('delta_km_hr')); ?>:</b>
	<?php echo CHtml::encode($data->delta_km_hr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('status')); ?>:</b>
	<?php echo CHtml::encode($data->status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('speedometer')); ?>:</b>
	<?php echo CHtml::encode($data->speedometer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('from')); ?>:</b>
	<?php echo CHtml::encode($data->from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('origin_docs')); ?>:</b>
	<?php echo CHtml::encode($data->origin_docs); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('customs_declaration')); ?>:</b>
	<?php echo CHtml::encode($data->customs_declaration); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('im_status')); ?>:</b>
	<?php echo CHtml::encode($data->im_status); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('custom_declaration_export')); ?>:</b>
	<?php echo CHtml::encode($data->custom_declaration_export); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('im_40')); ?>:</b>
	<?php echo CHtml::encode($data->im_40); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('danger_perm')); ?>:</b>
	<?php echo CHtml::encode($data->danger_perm); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('temp_registr')); ?>:</b>
	<?php echo CHtml::encode($data->temp_registr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tech_from')); ?>:</b>
	<?php echo CHtml::encode($data->tech_from); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('till')); ?>:</b>
	<?php echo CHtml::encode($data->till); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sanitary')); ?>:</b>
	<?php echo CHtml::encode($data->sanitary); ?>
	<br />

	*/ ?>

</div>