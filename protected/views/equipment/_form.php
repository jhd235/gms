<?php
/* @var $this EquipmentController */
/* @var $model Equipment */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'equipment-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'company_code'); ?>
		<?php echo $form->textField($model,'company_code',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'company_code'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'category'); ?>
		<?php echo $form->textField($model,'category',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'category'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>200)); ?>
		<?php echo $form->error($model,'description'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
		<?php echo $form->error($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'imp'); ?>
		<?php echo $form->textField($model,'imp'); ?>
		<?php echo $form->error($model,'imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'si'); ?>
		<?php echo $form->textField($model,'si'); ?>
		<?php echo $form->error($model,'si'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'s_n_chassis'); ?>
		<?php echo $form->textField($model,'s_n_chassis',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'s_n_chassis'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'s_n_engine'); ?>
		<?php echo $form->textField($model,'s_n_engine',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'s_n_engine'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'plate_number'); ?>
		<?php echo $form->textField($model,'plate_number',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'plate_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'year'); ?>
		<?php echo $form->textField($model,'year',array('size'=>4,'maxlength'=>4)); ?>
		<?php echo $form->error($model,'year'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'maintenance_status'); ?>
		<?php echo $form->textField($model,'maintenance_status',array('size'=>9,'maxlength'=>9)); ?>
		<?php echo $form->error($model,'maintenance_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_service_date'); ?>
		<?php echo $form->textField($model,'last_service_date'); ?>
		<?php echo $form->error($model,'last_service_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_service_hour_km'); ?>
		<?php echo $form->textField($model,'last_service_hour_km'); ?>
		<?php echo $form->error($model,'last_service_hour_km'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'last_recording_hrs_km'); ?>
		<?php echo $form->textField($model,'last_recording_hrs_km',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'last_recording_hrs_km'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'delta_km_hr'); ?>
		<?php echo $form->textField($model,'delta_km_hr'); ?>
		<?php echo $form->error($model,'delta_km_hr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'speedometer'); ?>
		<?php echo $form->textField($model,'speedometer'); ?>
		<?php echo $form->error($model,'speedometer'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'from'); ?>
		<?php echo $form->textField($model,'from',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'origin_docs'); ?>
		<?php echo $form->textField($model,'origin_docs',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'origin_docs'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'customs_declaration'); ?>
		<?php echo $form->textField($model,'customs_declaration',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'customs_declaration'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'im_status'); ?>
		<?php echo $form->textField($model,'im_status',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'im_status'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'custom_declaration_export'); ?>
		<?php echo $form->textField($model,'custom_declaration_export',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'custom_declaration_export'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'im_40'); ?>
		<?php echo $form->textField($model,'im_40'); ?>
		<?php echo $form->error($model,'im_40'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'danger_perm'); ?>
		<?php echo $form->textField($model,'danger_perm'); ?>
		<?php echo $form->error($model,'danger_perm'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'temp_registr'); ?>
		<?php echo $form->textField($model,'temp_registr'); ?>
		<?php echo $form->error($model,'temp_registr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tech_from'); ?>
		<?php echo $form->textField($model,'tech_from'); ?>
		<?php echo $form->error($model,'tech_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'till'); ?>
		<?php echo $form->textField($model,'till'); ?>
		<?php echo $form->error($model,'till'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sanitary'); ?>
		<?php echo $form->textField($model,'sanitary'); ?>
		<?php echo $form->error($model,'sanitary'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->