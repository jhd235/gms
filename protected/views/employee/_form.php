<?php
/* @var $this EmployeeController */
/* @var $model Employee */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'employee-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'e_name'); ?>
		<?php echo $form->textField($model,'e_name',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'e_name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'e_password'); ?>
		<?php echo $form->textField($model,'e_password',array('size'=>60,'maxlength'=>100)); ?>
		<?php echo $form->error($model,'e_password'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'e_email'); ?>
		<?php echo $form->textField($model,'e_email',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'e_email'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'e_status'); ?>
		<?php echo $form->textField($model,'e_status',array('size'=>10,'maxlength'=>10)); ?>
		<?php echo $form->error($model,'e_status'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->