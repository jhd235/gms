<?php
/* @var $this ManhoursController */
/* @var $model Manhours */

$this->breadcrumbs=array(
	'Manhours'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Manhours', 'url'=>array('index')),
	array('label'=>'Create Manhours', 'url'=>array('create')),
	array('label'=>'Update Manhours', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Manhours', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Manhours', 'url'=>array('admin')),
);
?>

<h1>View Manhours #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'item',
		'badge_number',
		'name',
		'position',
		'nationality',
		'note',
		'project_location',
		'shift',
		'hr',
		'contract',
		'earth_works',
		'civil_works',
		'steel_works',
		'piping_mechanical_works',
		'others_works',
		'remarks',
		'job_pack_number',
	),
)); ?>
