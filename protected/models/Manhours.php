<?php

/**
 * This is the model class for table "manhours".
 *
 * The followings are the available columns in table 'manhours':
 * @property integer $id
 * @property integer $item
 * @property string $badge_number
 * @property string $name
 * @property string $position
 * @property string $nationality
 * @property string $note
 * @property string $project_location
 * @property string $shift
 * @property integer $hr
 * @property string $contract
 * @property integer $earth_works
 * @property integer $civil_works
 * @property integer $steel_works
 * @property integer $piping_mechanical_works
 * @property integer $others_works
 * @property integer $remarks
 * @property integer $job_pack_number
 */
class Manhours extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'manhours';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('item, name, position, nationality, project_location, shift, hr, contract, others_works', 'required'),
			array('item, hr, earth_works, civil_works, steel_works, piping_mechanical_works, others_works, remarks, job_pack_number', 'numerical', 'integerOnly'=>true),
			array('badge_number, position, nationality, note, project_location, shift', 'length', 'max'=>40),
			array('name', 'length', 'max'=>100),
			array('contract', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, item, badge_number, name, position, nationality, note, project_location, shift, hr, contract, earth_works, civil_works, steel_works, piping_mechanical_works, others_works, remarks, job_pack_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'item' => 'Item',
			'badge_number' => 'Badge Number',
			'name' => 'Name',
			'position' => 'Position',
			'nationality' => 'Nationality',
			'note' => 'Note',
			'project_location' => 'Project Location',
			'shift' => 'Shift',
			'hr' => 'Hr',
			'contract' => 'Contract',
			'earth_works' => 'Earth Works',
			'civil_works' => 'Civil Works',
			'steel_works' => 'Steel Works',
			'piping_mechanical_works' => 'Piping Mechanical Works',
			'others_works' => 'Others Works',
			'remarks' => 'Remarks',
			'job_pack_number' => 'Job Pack Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('item',$this->item);
		$criteria->compare('badge_number',$this->badge_number,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('position',$this->position,true);
		$criteria->compare('nationality',$this->nationality,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('project_location',$this->project_location,true);
		$criteria->compare('shift',$this->shift,true);
		$criteria->compare('hr',$this->hr);
		$criteria->compare('contract',$this->contract,true);
		$criteria->compare('earth_works',$this->earth_works);
		$criteria->compare('civil_works',$this->civil_works);
		$criteria->compare('steel_works',$this->steel_works);
		$criteria->compare('piping_mechanical_works',$this->piping_mechanical_works);
		$criteria->compare('others_works',$this->others_works);
		$criteria->compare('remarks',$this->remarks);
		$criteria->compare('job_pack_number',$this->job_pack_number);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Manhours the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
   /* public function scopes()
    {
        return array(
            'lastRecord'=>array(
                'condition'=>'id = '.Yii::app()->user->id.' AND status = 1',
                'order'=>'orderField DESC',
                'limit'=>1,
            ),
        );
    }*/
}
