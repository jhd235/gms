<?php
/* @var $this ManhoursController */
/* @var $model Manhours */
/* @var $form CActiveForm */
/* @var $modelLaborer Laborer*/
$criteria=new CDbCriteria;

$criteria->select='name, job_position';  // only select the 'AlId' and 'AlDescr' columns

$qname=Laborer::model()->findAll($criteria);

$laborerName = array();

foreach($qname as $p)

{

    $laborerName[$p->name] = $p->name;
    $laborerJP[$p->job_position] = $p->job_position;

}

?>
there are data from Laborer database
<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'manhours-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'item'); ?>
        <?php //echo $model = Manhours::model()->lastRecord()->find();?>
        <?php echo $form->textField($model,'item'); ?>
		<?php echo $form->error($model,'item'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'badge_number'); ?>
		<?php echo $form->textField($model,'badge_number',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'badge_number'); ?>
	</div>

	<div class="row">
        <?php echo $form->labelEx($model,'name'); ?>
        <?php
        $this->widget('CAutoComplete',
            array(
//name of the html field that will be generated
                'name'=>'name',
//replace controller/action with real ids
                'url'=>array('manhours/AutoCompleteLookup'),
                'max'=>10, //specifies the max number of items to display

//specifies the number of chars that must be entered
//before autocomplete initiates a lookup
                'minChars'=>2,
                'delay'=>500, //number of milliseconds before lookup occurs
                'matchCase'=>false, //match case when performing a lookup?

//any additional html attributes that go inside of
//the input field can be defined here
                'htmlOptions'=>array('size'=>'40', 'name'=>'Manhours[name]'),

                'methodChain'=>".result(function(event,item){\$(\"#id\").val(item[1]);})",
            ));
        ?>
        <?php echo CHtml::hiddenField('id'); ?>

        <?php //echo CHtml::activeDropDownList($modelLaborer, 'name', $laborerName);?>
        <?php //echo $form->textField($model,'name', array('size'=>60,'maxlength'=>100, 'value'=>CHtml::activeDropDownList($modelLaborer, 'name', $laborerName))); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'position'); ?>
        <?php
        $this->widget('CAutoComplete',
            array(
//name of the html field that will be generated
                'name'=>'position',
//replace controller/action with real ids
                'url'=>array('manhours/AutoCompleteLookupPosition'),
                'max'=>10, //specifies the max number of items to display

//specifies the number of chars that must be entered
//before autocomplete initiates a lookup
                'minChars'=>2,
                'delay'=>500, //number of milliseconds before lookup occurs
                'matchCase'=>false, //match case when performing a lookup?

//any additional html attributes that go inside of
//the input field can be defined here
                'htmlOptions'=>array('size'=>'40', 'name'=>'Manhours[position]'),

                'methodChain'=>".result(function(event,item){\$(\"#id\").val(item[1]);})",
            ));
        ?>
        <?php echo CHtml::hiddenField('id'); ?>


        <?php //echo $form->textField($model,'position',array('size'=>40,'maxlength'=>40)); ?>
        <?php //echo CHtml::activeDropDownList($modelLaborer, 'job_position', $laborerJP);?>
		<?php echo $form->error($model,'position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'nationality'); ?>
		<?php echo $form->textField($model,'nationality',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'nationality'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'note'); ?>
		<?php echo $form->textField($model,'note',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'note'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'project_location'); ?>
		<?php echo $form->textField($model,'project_location',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'project_location'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'shift'); ?>
		<?php echo $form->textField($model,'shift',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'shift'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'hr'); ?>
		<?php echo $form->textField($model,'hr'); ?>
		<?php echo $form->error($model,'hr'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contract'); ?>
		<?php echo $form->textField($model,'contract',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'contract'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'earth_works'); ?>
		<?php echo $form->textField($model,'earth_works'); ?>
		<?php echo $form->error($model,'earth_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'civil_works'); ?>
		<?php echo $form->textField($model,'civil_works'); ?>
		<?php echo $form->error($model,'civil_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'steel_works'); ?>
		<?php echo $form->textField($model,'steel_works'); ?>
		<?php echo $form->error($model,'steel_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'piping_mechanical_works'); ?>
		<?php echo $form->textField($model,'piping_mechanical_works'); ?>
		<?php echo $form->error($model,'piping_mechanical_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'others_works'); ?>
		<?php echo $form->textField($model,'others_works'); ?>
		<?php echo $form->error($model,'others_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks'); ?>
		<?php echo $form->error($model,'remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_pack_number'); ?>
		<?php echo $form->textField($model,'job_pack_number'); ?>
		<?php echo $form->error($model,'job_pack_number'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->