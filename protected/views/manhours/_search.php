<?php
/* @var $this ManhoursController */
/* @var $model Manhours */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'item'); ?>
		<?php echo $form->textField($model,'item'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'badge_number'); ?>
		<?php echo $form->textField($model,'badge_number',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'position'); ?>
		<?php echo $form->textField($model,'position',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'nationality'); ?>
		<?php echo $form->textField($model,'nationality',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note'); ?>
		<?php echo $form->textField($model,'note',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'project_location'); ?>
		<?php echo $form->textField($model,'project_location',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'shift'); ?>
		<?php echo $form->textField($model,'shift',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'hr'); ?>
		<?php echo $form->textField($model,'hr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contract'); ?>
		<?php echo $form->textField($model,'contract',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'earth_works'); ?>
		<?php echo $form->textField($model,'earth_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'civil_works'); ?>
		<?php echo $form->textField($model,'civil_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'steel_works'); ?>
		<?php echo $form->textField($model,'steel_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'piping_mechanical_works'); ?>
		<?php echo $form->textField($model,'piping_mechanical_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'others_works'); ?>
		<?php echo $form->textField($model,'others_works'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'remarks'); ?>
		<?php echo $form->textField($model,'remarks'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_pack_number'); ?>
		<?php echo $form->textField($model,'job_pack_number'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->