<?php
/* @var $this ManhoursController */
/* @var $model Manhours */

$this->breadcrumbs=array(
	'Manhours'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List Manhours', 'url'=>array('index')),
	array('label'=>'Create Manhours', 'url'=>array('create')),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$('#manhours-grid').yiiGridView('update', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Manhours</h1>

<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>

<?php echo CHtml::link('Advanced Search','#',array('class'=>'search-button')); ?>
<div class="search-form" style="display:none">
<?php $this->renderPartial('_search',array(
	'model'=>$model,
)); ?>
</div><!-- search-form -->

<?php $this->widget('zii.widgets.grid.CGridView', array(
	'id'=>'manhours-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'id',
		'item',
		'badge_number',
		'name',
		'position',
		'nationality',
		/*
		'note',
		'project_location',
		'shift',
		'hr',
		'contract',
		'earth_works',
		'civil_works',
		'steel_works',
		'piping_mechanical_works',
		'others_works',
		'remarks',
		'job_pack_number',
		*/
		array(
			'class'=>'CButtonColumn',
		),
	),
)); ?>
