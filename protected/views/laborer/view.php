<?php
/* @var $this LaborerController */
/* @var $model Laborer */

$this->breadcrumbs=array(
	'Laborers'=>array('index'),
	$model->name,
);

$this->menu=array(
	array('label'=>'List Laborer', 'url'=>array('index')),
	array('label'=>'Create Laborer', 'url'=>array('create')),
	array('label'=>'Update Laborer', 'url'=>array('update', 'id'=>$model->id)),
	array('label'=>'Delete Laborer', 'url'=>'#', 'linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?')),
	array('label'=>'Manage Laborer', 'url'=>array('admin')),
);
?>

<h1>View Laborer #<?php echo $model->id; ?></h1>

<?php $this->widget('zii.widgets.CDetailView', array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'name',
		'date_of_birth',
		'place_of_birth',
		'id_number',
		'job_position',
		'date_of_hiring',
		'kin_of_contract',
		'contact_expiring_date',
		'place_of_work',
		'tco_badge_number',
		'sicim_badge_number',
	),
)); ?>
