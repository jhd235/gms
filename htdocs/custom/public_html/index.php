<!DOCTYPE html>
<html lang="es" xmlns="http://www.w3.org/1999/html">
<head>
	<title>SICIM Tengiz Base</title>
	<meta charset="utf-8" />
	
	<link rel="stylesheet" href="css/main.css" type="text/css" />

	<!--[if IE]>
		<script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script><![endif]-->
	
	<!--[if lte IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="css/ie.css"/>
		<script src="js/IE8.js" type="text/javascript"></script><![endif]-->
		
	<!--[if lt IE 7]>
		<link rel="stylesheet" type="text/css" media="all" href="css/ie6.css"/><![endif]-->
    <script src="js/common.js"></script>
    <script>
        $(function() {
            //document.getElementById("login").style.visibility = "hidden";
            document.getElementById("login").style.display = "none";
        });

        function changeContent(){
            var content = document.getElementById("content");
                //content.visibility = false;
            content.style.display = "none";
            document.getElementById("login").style.display = "block";
        }
    </script>

</head>

<body id="index" class="home">
	<div id="fixed" style="
	position: fixed;
top: 0;
left: 0;
padding: 10px 0 0 0;
background-color: rgba(255, 255, 255, .9);
">
	
	<ul style="display: block">
		<li>Home</li>
		<li>Administration</li>
		<li>Bugtrack</li>
	</ul>
</div>
	<header id="banner" class="body">
		<h1><a href="#">DTE0001 <strong>DTE0001 <del>Supervisor weekly planning</del> <ins>sitten_equip</ins></strong></a></h1>
		<nav><ul>
			<li class="active"><a href="#">home</a></li>
			<li><a id="LoginLink" href="#" onclick="changeContent()">login</a></li>            
			<li><a href="#">bugtrack</a></li>
			<li><a href="#">contact</a></li>
		</ul></nav>
	</header><!-- /#banner -->
	
	<aside id="featured" class="body"><article>
		<figure>
			<img src="img/sicim_logo_small.jpg" alt="sicim_logo" />
		</figure>
		<hgroup>
			<h2>Tools and equipment picking ticket</h2>
			<h3><a href="#">for SICIM internal use only</a></h3>
		</hgroup>
		<p>Ticket emission tool <a href="#" rel="external">SICIM</a> and <a href="#" rel="external">TCO</a> .</p>
	</article></aside><!-- /#featured -->

    <div id="login" class="body">
        <table width="300" border="0" align="center" cellpadding="0" cellspacing="1" bgcolor="#CCCCCC">
            <tr>
                <form name="form1" method="post" action="admin/checklogin.php">
                    <td>
                        <table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#FFFFFF">
                            <tr>
                                <td colspan="3"><strong>Member Login </strong></td>
                            </tr>
                            <tr>
                                <td width="78">Email</td>
                                <td width="6">:</td>
                                <td width="294"><input name="myusername" type="text" id="myusername"></td>
                            </tr>
                            <tr>
                                <td>Password</td>
                                <td>:</td>
                                <td><input name="mypassword" type="password" id="mypassword"></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td>&nbsp;</td>
                                <td><input type="submit" name="Submit" value="Login"></td>                                
                            </tr>
                            <tr><td><a href="admin/regi/">Register</td></tr>
                        </table>
                    </td>
                </form>
            </tr>
        </table>
        <div style="text-align: center">
            <a href="/">Main page</a>
        </div>

    </div>
	
	<section id="content" class="body">
		
		<ol id="posts-list" class="hfeed">
			<li><article class="hentry">	
				<header>
					<h2 class="entry-title"><a href="#" rel="bookmark" title="Permalink to this POST TITLE">There are will be instructions</a></h2>
				</header>
				
				<footer class="post-info">
					<abbr class="published" title="2005-10-10T14:07:00-07:00"><!-- YYYYMMDDThh:mm:ss+ZZZZ -->
						15th February 2014
					</abbr>

					<address class="vcard author">
						By <a class="url fn" href="#">HSE department</a>
					</address>
				</footer><!-- /.post-info -->
				
				<div class="entry-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque venenatis nunc vitae libero iaculis elementum. Nullam et justo <a href="#">non sapien</a> dapibus blandit nec et leo. Ut ut malesuada tellus.</p>
				</div><!-- /.entry-content -->
			</article></li>
			<li><article class="hentry">	
				<header>
					<h2 class="entry-title"><a href="#" rel="bookmark" title="Permalink to this POST TITLE">There are will be instructions</a></h2>
				</header>
				
				<footer class="post-info">
					<abbr class="published" title="2005-10-10T14:07:00-07:00"><!-- YYYYMMDDThh:mm:ss+ZZZZ -->
						15th February 2014
					</abbr>

					<address class="vcard author">
						By <a class="url fn" href="#">HSE department</a>
					</address>
				</footer><!-- /.post-info -->
				
				<div class="entry-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque venenatis nunc vitae libero iaculis elementum. Nullam et justo <a href="#">non sapien</a> dapibus blandit nec et leo. Ut ut malesuada tellus.</p>
				</div><!-- /.entry-content -->
			</article></li>
			<li><article class="hentry">	
				<header>
					<h2 class="entry-title"><a href="#" rel="bookmark" title="Permalink to this POST TITLE">There are will be instructions</a></h2>
				</header>
				
				<footer class="post-info">
					<abbr class="published" title="2005-10-10T14:07:00-07:00"><!-- YYYYMMDDThh:mm:ss+ZZZZ -->
						15th February 2014
					</abbr>

					<address class="vcard author">
						By <a class="url fn" href="#">HSE department</a>
					</address>
				</footer><!-- /.post-info -->
				
				<div class="entry-content">
					<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Quisque venenatis nunc vitae libero iaculis elementum. Nullam et justo <a href="#">non sapien</a> dapibus blandit nec et leo. Ut ut malesuada tellus.</p>
				</div><!-- /.entry-content -->
			</article></li>
		</ol><!-- /#posts-list -->
		
	</section><!-- /#content -->
	
	<section id="extras" class="body">
		<div class="blogroll">
			<h2>Internal departments</h2>
			<ul>
				<li><a href="#" rel="external">HSE department</a></li>
				<li><a href="#" rel="external">HR department</a></li>
				<li><a href="#" rel="external">Technical department</a></li>
				<li><a href="#" rel="external">QA/QA</a></li>
				<li><a href="#" rel="external">Project manager</a></li>
				<li><a href="#" rel="external">Site manager</a></li>
				
				<li><a href="#" rel="external">HSE department</a></li>
				<li><a href="#" rel="external">HR department</a></li>
				<li><a href="#" rel="external">Technical department</a></li>
				<li><a href="#" rel="external">QA/QA</a></li>
				<li><a href="#" rel="external">Project manager</a></li>
				<li><a href="#" rel="external">Site manager</a></li>
				
				<li><a href="#" rel="external">HSE department</a></li>
				<li><a href="#" rel="external">HR department</a></li>
				<li><a href="#" rel="external">Technical department</a></li>
				<li><a href="#" rel="external">QA/QA</a></li>
				<li><a href="#" rel="external">Project manager</a></li>
				<li><a href="#" rel="external">Site manager</a></li>
			</ul>
		</div><!-- /.blogroll -->
		
		<div class="social">
			<h2>social</h2>
			<ul>
				<li><a href="#" rel="me">delicious</a></li>
				<li><a href="#" rel="me">digg</a></li>
				<li><a href="#" rel="me">facebook</a></li>
				<li><a href="#" rel="me">last.fm</a></li>
				<li><a href="#" rel="alternate">rss</a></li>
				<li><a href="#" rel="me">twitter</a></li>
			</ul>
		</div><!-- /.social -->
	</section><!-- /#extras -->
	
	<footer id="contentinfo" class="body">
		<address id="about" class="vcard body">
			<span class="primary">
				<strong><a href="#" class="fn url">SICIM s.p.a.</a></strong>
				<span class="role">IT department</span>
			</span><!-- /.primary -->
		
			<img src="img/sicim_logo_small.jpg" alt="SICIM s.p.a." class="logo" />
		
			<span class="bio">there will be few words about company.</span>
		
		</address><!-- /#about -->
		
		<p>1962-2014 <a href="http://www.sicim.eu">SICIM</a>.</p>
	</footer><!-- /#contentinfo -->

</body>
</html>
