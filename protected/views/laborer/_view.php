<?php
/* @var $this LaborerController */
/* @var $data Laborer */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_of_birth')); ?>:</b>
	<?php echo CHtml::encode($data->date_of_birth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_of_birth')); ?>:</b>
	<?php echo CHtml::encode($data->place_of_birth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('id_number')); ?>:</b>
	<?php echo CHtml::encode($data->id_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_position')); ?>:</b>
	<?php echo CHtml::encode($data->job_position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('date_of_hiring')); ?>:</b>
	<?php echo CHtml::encode($data->date_of_hiring); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('kin_of_contract')); ?>:</b>
	<?php echo CHtml::encode($data->kin_of_contract); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contact_expiring_date')); ?>:</b>
	<?php echo CHtml::encode($data->contact_expiring_date); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('place_of_work')); ?>:</b>
	<?php echo CHtml::encode($data->place_of_work); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('tco_badge_number')); ?>:</b>
	<?php echo CHtml::encode($data->tco_badge_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('sicim_badge_number')); ?>:</b>
	<?php echo CHtml::encode($data->sicim_badge_number); ?>
	<br />

	*/ ?>

</div>