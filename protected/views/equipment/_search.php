<?php
/* @var $this EquipmentController */
/* @var $model Equipment */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'company_code'); ?>
		<?php echo $form->textField($model,'company_code',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'category'); ?>
		<?php echo $form->textField($model,'category',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'description'); ?>
		<?php echo $form->textField($model,'description',array('size'=>60,'maxlength'=>200)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'quantity'); ?>
		<?php echo $form->textField($model,'quantity'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'imp'); ?>
		<?php echo $form->textField($model,'imp'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'si'); ?>
		<?php echo $form->textField($model,'si'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_n_chassis'); ?>
		<?php echo $form->textField($model,'s_n_chassis',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'s_n_engine'); ?>
		<?php echo $form->textField($model,'s_n_engine',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'plate_number'); ?>
		<?php echo $form->textField($model,'plate_number',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'year'); ?>
		<?php echo $form->textField($model,'year',array('size'=>4,'maxlength'=>4)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'location'); ?>
		<?php echo $form->textField($model,'location',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'maintenance_status'); ?>
		<?php echo $form->textField($model,'maintenance_status',array('size'=>9,'maxlength'=>9)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_service_date'); ?>
		<?php echo $form->textField($model,'last_service_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_service_hour_km'); ?>
		<?php echo $form->textField($model,'last_service_hour_km'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'last_recording_hrs_km'); ?>
		<?php echo $form->textField($model,'last_recording_hrs_km',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'delta_km_hr'); ?>
		<?php echo $form->textField($model,'delta_km_hr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'status'); ?>
		<?php echo $form->textField($model,'status',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'speedometer'); ?>
		<?php echo $form->textField($model,'speedometer'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'from'); ?>
		<?php echo $form->textField($model,'from',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'origin_docs'); ?>
		<?php echo $form->textField($model,'origin_docs',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'customs_declaration'); ?>
		<?php echo $form->textField($model,'customs_declaration',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'im_status'); ?>
		<?php echo $form->textField($model,'im_status',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'custom_declaration_export'); ?>
		<?php echo $form->textField($model,'custom_declaration_export',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'note'); ?>
		<?php echo $form->textField($model,'note',array('size'=>60,'maxlength'=>100)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'im_40'); ?>
		<?php echo $form->textField($model,'im_40'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'danger_perm'); ?>
		<?php echo $form->textField($model,'danger_perm'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'temp_registr'); ?>
		<?php echo $form->textField($model,'temp_registr'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tech_from'); ?>
		<?php echo $form->textField($model,'tech_from'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'till'); ?>
		<?php echo $form->textField($model,'till'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sanitary'); ?>
		<?php echo $form->textField($model,'sanitary'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->