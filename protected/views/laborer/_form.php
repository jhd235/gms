<?php
/* @var $this LaborerController */
/* @var $model Laborer */
/* @var $form CActiveForm */
?>

<div class="form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'laborer-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// There is a call to performAjaxValidation() commented in generated controller code.
	// See class documentation of CActiveForm for details on this.
	'enableAjaxValidation'=>false,
)); ?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">
		<?php echo $form->labelEx($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'name'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_of_birth'); ?>
		<?php echo $form->textField($model,'date_of_birth'); ?>
		<?php echo $form->error($model,'date_of_birth'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'place_of_birth'); ?>
		<?php echo $form->textField($model,'place_of_birth',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'place_of_birth'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'id_number'); ?>
		<?php echo $form->textField($model,'id_number'); ?>
		<?php echo $form->error($model,'id_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'job_position'); ?>
		<?php echo $form->textField($model,'job_position',array('size'=>40,'maxlength'=>40)); ?>
		<?php echo $form->error($model,'job_position'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'date_of_hiring'); ?>
		<?php echo $form->textField($model,'date_of_hiring'); ?>
		<?php echo $form->error($model,'date_of_hiring'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'kin_of_contract'); ?>
		<?php echo $form->textField($model,'kin_of_contract',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'kin_of_contract'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'contact_expiring_date'); ?>
		<?php echo $form->textField($model,'contact_expiring_date'); ?>
		<?php echo $form->error($model,'contact_expiring_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'place_of_work'); ?>
		<?php echo $form->textField($model,'place_of_work',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'place_of_work'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'tco_badge_number'); ?>
		<?php echo $form->textField($model,'tco_badge_number',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'tco_badge_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->labelEx($model,'sicim_badge_number'); ?>
		<?php echo $form->textField($model,'sicim_badge_number',array('size'=>20,'maxlength'=>20)); ?>
		<?php echo $form->error($model,'sicim_badge_number'); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton($model->isNewRecord ? 'Create' : 'Save'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->