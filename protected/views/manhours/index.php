<?php
/* @var $this ManhoursController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Manhours',
);

$this->menu=array(
	array('label'=>'Create Manhours', 'url'=>array('create')),
	array('label'=>'Manage Manhours', 'url'=>array('admin')),
);
?>

<h1>Manhours</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
