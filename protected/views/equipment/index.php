<?php
/* @var $this EquipmentController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Equipments',
);

$this->menu=array(
	array('label'=>'Create Equipment', 'url'=>array('create')),
	array('label'=>'Manage Equipment', 'url'=>array('admin')),
);
?>

<h1>Equipments</h1>

<?php $this->widget('zii.widgets.CListView', array(
//$this->widget('zii.widgets.grid.CGridView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
    //'id'=>'equipment-grid',
    //'dataProvider'=>$model->search(),
    //'filter'=>$model,
   /* 'columns'=>array(
        'id',
        'plate_number',
        'category',
        'description',
        'quantity',
        'imp',
     */   /*
        'si',
        's_n_chassis',
        's_n_engine',
        'plate_number',
        'year',
        'location',
        'maintenance_status',
        'last_service_date',
        'last_service_hour_km',
        'last_recording_hrs_km',
        'delta_km_hr',
        'status',
        'speedometer',
        'from',
        'origin_docs',
        'customs_declaration',
        'im_status',
        'custom_declaration_export',
        'note',
        'im_40',
        'danger_perm',
        'temp_registr',
        'tech_from',
        'till',
        'sanitary',
        */

    ));

?>