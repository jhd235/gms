<?php
/* @var $this LaborerController */
/* @var $model Laborer */

$this->breadcrumbs=array(
	'Laborers'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Laborer', 'url'=>array('index')),
	array('label'=>'Manage Laborer', 'url'=>array('admin')),
);
?>

<h1>Create Laborer</h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>