<?php
/* @var $this EmployeeController */
/* @var $data Employee */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('e_name')); ?>:</b>
	<?php echo CHtml::encode($data->e_name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('e_password')); ?>:</b>
	<?php echo CHtml::encode($data->e_password); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('e_email')); ?>:</b>
	<?php echo CHtml::encode($data->e_email); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('e_status')); ?>:</b>
	<?php echo CHtml::encode($data->e_status); ?>
	<br />


</div>