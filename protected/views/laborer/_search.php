<?php
/* @var $this LaborerController */
/* @var $model Laborer */
/* @var $form CActiveForm */
?>

<div class="wide form">

<?php $form=$this->beginWidget('CActiveForm', array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="row">
		<?php echo $form->label($model,'id'); ?>
		<?php echo $form->textField($model,'id'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'name'); ?>
		<?php echo $form->textField($model,'name',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_of_birth'); ?>
		<?php echo $form->textField($model,'date_of_birth'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'place_of_birth'); ?>
		<?php echo $form->textField($model,'place_of_birth',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'id_number'); ?>
		<?php echo $form->textField($model,'id_number'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'job_position'); ?>
		<?php echo $form->textField($model,'job_position',array('size'=>40,'maxlength'=>40)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'date_of_hiring'); ?>
		<?php echo $form->textField($model,'date_of_hiring'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'kin_of_contract'); ?>
		<?php echo $form->textField($model,'kin_of_contract',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'contact_expiring_date'); ?>
		<?php echo $form->textField($model,'contact_expiring_date'); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'place_of_work'); ?>
		<?php echo $form->textField($model,'place_of_work',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'tco_badge_number'); ?>
		<?php echo $form->textField($model,'tco_badge_number',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row">
		<?php echo $form->label($model,'sicim_badge_number'); ?>
		<?php echo $form->textField($model,'sicim_badge_number',array('size'=>20,'maxlength'=>20)); ?>
	</div>

	<div class="row buttons">
		<?php echo CHtml::submitButton('Search'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- search-form -->