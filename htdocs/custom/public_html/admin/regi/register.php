<?php
session_start();
// we check if everything is filled in
if(empty($_POST['firstname']) || empty($_POST['lastname']) || empty($_POST['reg_email__']) || empty($_POST['reg_passwd__']))
{
	die(msg(0,"You must fill in all of the fields."));
}

// is the email valid?
if(!(preg_match("/^[\.A-z0-9_\-\+]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z]{1,4}$/", $_POST['reg_email__'])))
	die(msg(0,"Please enter a valid email address."));

if( $_POST['reg_email__'] != $_POST['reg_email_confirmation__']){
	die(msg(0,"Your email addresses do not match. Please try again."));
	
}

// is the birthday selected?
if(!(int)$_POST['birthday_day'] || !(int)$_POST['birthday_month'] || !(int)$_POST['birthday_year'])
{
	die(msg(0,"You must indicate your full date of birth to register."));
}

// is the sex selected?
if(!(int)$_POST['sex'])
{
	die(msg(0,"Please select either Male or Female."));
}
require_once('config.php');

$firstName = mysql_real_escape_string($_POST['firstname']);
$lastName = mysql_real_escape_string($_POST['lastname']);
$email = mysql_real_escape_string($_POST['reg_email__']);
$password = mysql_real_escape_string($_POST['reg_passwd__']);
$password = md5($password);
$sex = mysql_real_escape_string($_POST['sex']);
$birthDay = mysql_real_escape_string($_POST['birthday_day']);
$birthMonth = date("F", mktime(0, 0, 0, mysql_real_escape_string($_POST['birthday_month']), 1, 2000));
$birthYear = mysql_real_escape_string($_POST['birthday_year']);
$birthDate = $birthMonth.' '.$birthDay.', '.$birthYear;

$select = mysql_query("SELECT email FROM fbregistration where email='".$_POST['reg_email__']."'");
if(mysql_num_rows($select) > 0){
	die(msg(0,"Sorry, it looks like $email belongs to an existing account. Would you like to <a href=''>claim this email address</a>?"));
}else{
    $insert = mysql_query("INSERT INTO fbregistration (firstName, lastName, email, password, gender, birthday) values('$firstName', '$lastName', '$email', '$password', '$sex', '$birthDate')");
	$_SESSION['LOGEDIN'] = $email;
}	

echo msg(1,"home.php");


function msg($status,$txt)
{
	return '{"status":'.$status.',"txt":"'.$txt.'"}';
}
?>