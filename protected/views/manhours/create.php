<?php
/* @var $this ManhoursController */
/* @var $model Manhours */
/* @var $modelLaborer Laborer */
$this->breadcrumbs=array(
	'Manhours'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Manhours', 'url'=>array('index')),
	array('label'=>'Manage Manhours', 'url'=>array('admin')),
);
?>

<h1>Create Manhours</h1>

<?php $this->renderPartial('_form', array('model'=>$model, 'modelLaborer'=>$modelLaborer)); ?>
