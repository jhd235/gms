<?php
session_start();
if(!session_is_registered(myusername)){
    header("location:index.php");
}
?>

<!DOCTYPE HTML>
<html>
<head>
<title>
	Adding laborers info to database
</title>
<link rel="stylesheet" type="text/css" href="css/main.css">
</head>
<body style="background-color: #F5F4EF">
	
<div id="nav" style="text-align: center; margin-top: 5%; background-color: #F5F4EF">
	Adding laborers info to database<br>
	
</div>
<div id="admin" style = "width:880px; margin:0 auto; text-align:center; background-color: #F5F4EF">
<?php
require_once 'add_laborerfrm.php';
?>
<a href="/">Main</a><br>
<a href="login_success.php">Administration</a><br>
<a href="../public/show_laborers.php">List laborers</a><br>
<a href="logout.php">Logout</a>
</div>
</body>
</html>
