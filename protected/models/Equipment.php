<?php

/**
 * This is the model class for table "equipment".
 *
 * The followings are the available columns in table 'equipment':
 * @property integer $id
 * @property string $company_code
 * @property string $category
 * @property string $description
 * @property integer $quantity
 * @property integer $imp
 * @property integer $si
 * @property string $s_n_chassis
 * @property string $s_n_engine
 * @property string $plate_number
 * @property string $year
 * @property string $location
 * @property string $maintenance_status
 * @property string $last_service_date
 * @property integer $last_service_hour_km
 * @property string $last_recording_hrs_km
 * @property integer $delta_km_hr
 * @property string $status
 * @property integer $speedometer
 * @property string $from
 * @property string $origin_docs
 * @property string $customs_declaration
 * @property string $im_status
 * @property string $custom_declaration_export
 * @property string $note
 * @property integer $im_40
 * @property integer $danger_perm
 * @property integer $temp_registr
 * @property integer $tech_from
 * @property string $till
 * @property integer $sanitary
 */
class Equipment extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'equipment';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('company_code, description, quantity, si, s_n_chassis, s_n_engine, plate_number, year, location, maintenance_status, delta_km_hr, ', 'required'),
			array('quantity, imp, si, last_service_hour_km, delta_km_hr, speedometer, im_40, danger_perm, temp_registr, tech_from, sanitary', 'numerical', 'integerOnly'=>true),
			array('company_code, from', 'length', 'max'=>20),
			array('category, s_n_chassis, s_n_engine, plate_number', 'length', 'max'=>40),
			array('description', 'length', 'max'=>200),
			array('year', 'length', 'max'=>4),
			array('location, last_recording_hrs_km, status, origin_docs, customs_declaration, im_status, custom_declaration_export, note', 'length', 'max'=>100),
			array('maintenance_status', 'length', 'max'=>9),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, company_code, category, description, quantity, imp, si, s_n_chassis, s_n_engine, plate_number, year, location, maintenance_status, last_service_date, last_service_hour_km, last_recording_hrs_km, delta_km_hr, status, speedometer, from, origin_docs, customs_declaration, im_status, custom_declaration_export, note, im_40, danger_perm, temp_registr, tech_from, till, sanitary', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'company_code' => 'Company Code',
			'category' => 'Category',
			'description' => 'Description',
			'quantity' => 'Quantity',
			'imp' => 'Imp',
			'si' => 'Si',
			's_n_chassis' => 'S N Chassis',
			's_n_engine' => 'S N Engine',
			'plate_number' => 'Plate Number',
			'year' => 'Year',
			'location' => 'Location',
			'maintenance_status' => 'Maintenance Status',
			'last_service_date' => 'Last Service Date',
			'last_service_hour_km' => 'Last Service Hour Km',
			'last_recording_hrs_km' => 'Last Recording Hrs Km',
			'delta_km_hr' => 'Delta Km Hr',
			'status' => 'Status',
			'speedometer' => 'Speedometer',
			'from' => 'From',
			'origin_docs' => 'Origin Docs',
			'customs_declaration' => 'Customs Declaration',
			'im_status' => 'Im Status',
			'custom_declaration_export' => 'Custom Declaration Export',
			'note' => 'Note',
			'im_40' => 'Im 40',
			'danger_perm' => 'Danger Perm',
			'temp_registr' => 'Temp Registr',
			'tech_from' => 'Tech From',
			'till' => 'Till',
			'sanitary' => 'Sanitary',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('company_code',$this->company_code,true);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('quantity',$this->quantity);
		$criteria->compare('imp',$this->imp);
		$criteria->compare('si',$this->si);
		$criteria->compare('s_n_chassis',$this->s_n_chassis,true);
		$criteria->compare('s_n_engine',$this->s_n_engine,true);
		$criteria->compare('plate_number',$this->plate_number,true);
		$criteria->compare('year',$this->year,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('maintenance_status',$this->maintenance_status,true);
		$criteria->compare('last_service_date',$this->last_service_date,true);
		$criteria->compare('last_service_hour_km',$this->last_service_hour_km);
		$criteria->compare('last_recording_hrs_km',$this->last_recording_hrs_km,true);
		$criteria->compare('delta_km_hr',$this->delta_km_hr);
		$criteria->compare('status',$this->status,true);
		$criteria->compare('speedometer',$this->speedometer);
		$criteria->compare('from',$this->from,true);
		$criteria->compare('origin_docs',$this->origin_docs,true);
		$criteria->compare('customs_declaration',$this->customs_declaration,true);
		$criteria->compare('im_status',$this->im_status,true);
		$criteria->compare('custom_declaration_export',$this->custom_declaration_export,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('im_40',$this->im_40);
		$criteria->compare('danger_perm',$this->danger_perm);
		$criteria->compare('temp_registr',$this->temp_registr);
		$criteria->compare('tech_from',$this->tech_from);
		$criteria->compare('till',$this->till,true);
		$criteria->compare('sanitary',$this->sanitary);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Equipment the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	public function behaviors() { return array( 'LoggableBehavior'=> 'application.modules.auditTrail.behaviors.LoggableBehavior', ); }
    public function freeSearch($keyword)
    {

        /*Creating a new criteria for search*/
        $criteria = new CDbCriteria;

        $criteria->compare('plate_number', $keyword, true, 'OR');
        $criteria->compare('description', $keyword, true, 'OR');
        $criteria->compare('category', $keyword, true, 'OR');

        /*result limit*/
        $criteria->limit = 100;
        /*When we want to return model*/
        return  Equipment::model()->findAll($criteria);

        /*To return active dataprovider uncomment the following code*/
        /*
        return new CActiveDataProvider($this, array(
            'criteria'=>$criteria,
        ));
        */

    }
}
