<?php /* @var $this Controller */ ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />

	<!-- blueprint CSS framework -->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print" />
	<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection" />
	<![endif]-->

	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css" />

	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
</head>

<body>

<div class="container" id="page">

	<div id="header">
		<div id="logo"><?php echo CHtml::encode(Yii::app()->name); ?></div>
	</div><!-- header -->

	<div id="mainmenu">
		<?php $this->widget('zii.widgets.CMenu',array(
			'items'=>array(
				array('label'=>'Home', 'url'=>array('/site/index')),
				array('label'=>'Equipment', 'url'=>array('/equipment'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Laborer', 'url'=>array('/laborer'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Manhours', 'url'=>array('/manhours/admin'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'Audit', 'url'=>array('/auditTrail/admin'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'DTE001', 'url'=>array('/equipment/dte001'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'DTE002', 'url'=>array('/equipment/dte002'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'DTE003', 'url'=>array('/equipment/dte003'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'Testing playground', 'url'=>array('/equipment/form'),'visible'=>!Yii::app()->user->isGuest),
                array('label'=>'TODO', 'url'=>array('/todo'),'visible'=>!Yii::app()->user->isGuest),
				array('label'=>'About', 'url'=>array('/site/page', 'view'=>'about')),
				array('label'=>'Contact', 'url'=>array('/site/contact')),
                array('label'=>'Support', 'url'=>array('/site/page', 'view'=>'support')),
				array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
				array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
			),
		)); ?>
	</div><!-- mainmenu -->
	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('zii.widgets.CBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>

	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?><br/>
		All Rights Reserved.<br/>

	</div><!-- footer -->

</div><!-- page -->

</body>
</html>
