<?php
/* @var $this ManhoursController */
/* @var $data Manhours */
?>

<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('id')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->id), array('view', 'id'=>$data->id)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('item')); ?>:</b>
	<?php echo CHtml::encode($data->item); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('badge_number')); ?>:</b>
	<?php echo CHtml::encode($data->badge_number); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('name')); ?>:</b>
	<?php echo CHtml::encode($data->name); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('position')); ?>:</b>
	<?php echo CHtml::encode($data->position); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nationality')); ?>:</b>
	<?php echo CHtml::encode($data->nationality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('note')); ?>:</b>
	<?php echo CHtml::encode($data->note); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('project_location')); ?>:</b>
	<?php echo CHtml::encode($data->project_location); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('shift')); ?>:</b>
	<?php echo CHtml::encode($data->shift); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hr')); ?>:</b>
	<?php echo CHtml::encode($data->hr); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('contract')); ?>:</b>
	<?php echo CHtml::encode($data->contract); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('earth_works')); ?>:</b>
	<?php echo CHtml::encode($data->earth_works); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('civil_works')); ?>:</b>
	<?php echo CHtml::encode($data->civil_works); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('steel_works')); ?>:</b>
	<?php echo CHtml::encode($data->steel_works); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('piping_mechanical_works')); ?>:</b>
	<?php echo CHtml::encode($data->piping_mechanical_works); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('others_works')); ?>:</b>
	<?php echo CHtml::encode($data->others_works); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('remarks')); ?>:</b>
	<?php echo CHtml::encode($data->remarks); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('job_pack_number')); ?>:</b>
	<?php echo CHtml::encode($data->job_pack_number); ?>
	<br />

	*/ ?>

</div>