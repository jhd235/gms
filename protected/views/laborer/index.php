<?php
/* @var $this LaborerController */
/* @var $dataProvider CActiveDataProvider */

$this->breadcrumbs=array(
	'Laborers',
);

$this->menu=array(
	array('label'=>'Create Laborer', 'url'=>array('create')),
	array('label'=>'Manage Laborer', 'url'=>array('admin')),
);
?>

<h1>Laborers</h1>

<?php $this->widget('zii.widgets.CListView', array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
