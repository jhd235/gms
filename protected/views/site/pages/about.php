<?php
/* @var $this SiteController */

$this->pageTitle=Yii::app()->name . ' - About';
$this->breadcrumbs=array(
	'About',
);
?>
<h1>About</h1>

This is basic documentation how to work with this web application.
Once you have opened default web address of this web application, 
you should see the Home tab. Other tabs are: Equipment, About, Contact, Login.
You can go to tabs by clicking the name of the tab.
Home tab displays brief info related to this web application.
Equipment tab allows unathorized users to simply see info related to equipments in database.
Unathorized users deny to create new equipments records in DB, manage them and delete from DB.
About tab displays some additional info.
Contact tab offers to contact with us.
Login tab for people who have to manage DB contents. 
Authorized users allowed to login, create new equipments records in database, update existing records, delete records from DB.
Also, authorized users can perform simple and advanced search within the Equipments database.<br>
Glossary:<br>
DB - database<br>
Pre-alpha refers to all activities performed during the software project before testing. These activities can include
requirements analysis, software design, software development, and unit testing. In typical open source development,
there are several types of pre-alpha versions. Milestone versions include specific sets of functions and are released
as soon as the functionality is complete.
Try to use <a href="http://localhost/support/" target="blank">Support Forum</a> to find out answers to your questions or ask one.