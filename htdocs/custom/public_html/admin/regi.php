<form id="reg" method="post" name="reg">
    <div class="large_form" id="reg_form_box">
        <table cellpadding="1" cellspacing="0" class="uiGrid editor">
            <tbody>
            <tr>
                <td class="label"><label for="firstname">First Name:</label>
                </td>
                <td><div class="field_container">
                        <input class="inputtext" id="firstname" name="firstname" type="text">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label"><label for="lastname">Last Name:</label>
                </td>
                <td><div class="field_container">
                        <input class="inputtext" id="lastname" name="lastname" type="text">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label"><label for="reg_email__">Your email address:</label>
                </td>
                <td><div class="field_container">
                        <input autocomplete="off" class="inputtext" id="reg_email__" name="reg_email__" type="text">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label"><label for="reg_email_confirmation__">Reenter email address:</label>
                </td>
                <td><div class="field_container">
                        <input autocomplete="off" class="inputtext" id="reg_email_confirmation__" name="reg_email_confirmation__" type="text">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label"><label for="reg_passwd__">New Password:</label>
                </td>
                <td><div class="field_container">
                        <input autocomplete="off" class="inputtext" id="reg_passwd__" name="reg_passwd__" type="password" value="">
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">I am:</td>
                <td><div class="field_container">
                        <select class="select" id="sex" name="sex">
                            <option value="0">Select Gender:</option>
                            <option value="1">Female</option>
                            <option value="2">Male</option>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label">Birthday:</td>
                <td><div class="field_container">
                        <select class="" id="birthday_day" name="birthday_day">
                            <option value="0">Day:</option>
                        </select>
                        <select class="" id="birthday_month" name="birthday_month">
                            <option value="0">Month:</option>
                        </select>
                        <select class="" id="birthday_year" name="birthday_year">
                            <option value="0">Year:</option>
                        </select>
                    </div>
                </td>
            </tr>
            <tr>
                <td class="label"></td>
                <td><div id="birthday_warning">
                        <a href="http://www.blogger.com/blogger.g?blogID=5592072499374696609" rel="dialog" title="Click for more information">Why do I need to provide my date of birth?</a>
                    </div>
                </td>
            </tr>
            </tbody>
        </table>
        <div class="reg_btn clearfix">
            <label class="uiButton uiButtonSpecial" for="u89aj1_3">
                <input id="u89aj1_3" type="submit" value="Sign Up">
            </label>
            <br>
<span class="async_status" id="async_status" style="display: none;">
  <img alt="" class="img" height="11" src="https://s-static.ak.facebook.com/rsrc.php/v1/yb/r/GsNJNwuI-UM.gif" width="16">
 </span>

        </div>
    </div>
</form>