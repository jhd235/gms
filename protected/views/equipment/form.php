<?php
/* @var $this EquipmentController */
/* @var $model Equipment */
/* @var $form CActiveForm */
$criteria=new CDbCriteria;

$criteria->select='plate_number,description';  // only select the 'AlId' and 'AlDescr' columns

$qAlbums=Equipment::model()->findAll($criteria);

$albums = array();

foreach($qAlbums as $p)

{

    $albums[$p->plate_number] = $p->description;

}

//return $albums;
echo CHtml::link('search', array('equipment/freeSearch'));
echo "<br><br>";
echo CHtml::link('DTE001', array('equipment/dte001'));
echo "<br><br>";
echo CHtml::link('DTE002', array('equipment/dte002'));
echo "<br><br>";
echo CHtml::link('DTE003', array('equipment/dte003'));
echo "<br><br>";
?>


<div class="form">


<?php $form=$this->beginWidget('CActiveForm', array(
	'id'=>'equipment-form-form',
	// Please note: When you enable ajax validation, make sure the corresponding
	// controller action is handling ajax validation correctly.
	// See class documentation of CActiveForm for details on this,
	// you need to use the performAjaxValidation()-method described there.
	'enableAjaxValidation'=>false,
));

?>

	<p class="note">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

	<div class="row">

        <?php echo CHtml::activeDropDownList($model, 'plate_number', $albums);?>
		<?php echo $form->labelEx($model,'company_code'); ?>
		<?php echo $form->textField($model,'company_code'); ?>
		<?php echo $form->error($model,'company_code'); ?>
	</div>




	<div class="row buttons">
		<?php echo CHtml::submitButton('Submit'); ?>
	</div>

<?php $this->endWidget(); ?>

</div><!-- form -->