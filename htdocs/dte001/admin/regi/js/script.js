$(document).ready(function(){
	
	$('#reg').submit(function(e) {

		$('#async_status').show();
		$('#reg_error').css({opacity:0});
		$.ajax({
		type: "POST",
		url: "register.php",
		data: $('#reg').serialize(),
		dataType: "json",
		success: function(msg){
			
			if(parseInt(msg.status)==1)
			{
				window.location=msg.txt;
			}
			else if(parseInt(msg.status)==0)
			{
				$('#reg_error').removeClass();
				$('#reg_error').animate({opacity:1}, 500);
				$('#reg_error_inner').html(msg.txt);
			}
			
			$('#async_status').hide();
		}
		});
		e.preventDefault();
		
	});
	$("select#birthday_month").change(function(){
		var options = '';
		var selected = '';
		var birthDay = $("select#birthday_day").val();
		var birthMonth = $(this).val();
		var birthYear = $("select#birthday_year").val();
		options += '<option value="0">Day:</option>';
		if(birthMonth == 2){
		  if(checkleapyear(birthYear)){
			  if(birthDay <= 29){
				  for (var i = 1; i < 30; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 30; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		  }else{
			 if(birthDay <= 28){
				  for (var i = 1; i < 29; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 29; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		  }
		  
		}else if(birthMonth == 1 || birthMonth == 3 || birthMonth == 5 || birthMonth == 7 || birthMonth == 8 || birthMonth == 10 || birthMonth == 12){
			  if(birthDay <= 31){
				  for (var i = 1; i < 32; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 32; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		}else{
			if(birthDay <= 30){
				  for (var i = 1; i < 31; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 31; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		}
		$("select#birthday_day").html(options);
	});
	
	$("select#birthday_year").change(function(){
		var options = '';
		var selected = '';
		var birthDay = $("select#birthday_day").val();
		var birthMonth = $("select#birthday_month").val();
		var birthYear = $(this).val();
		options += '<option value="0">Day:</option>';
		if(birthMonth == 2){
		  if(checkleapyear(birthYear)){
			  if(birthDay <= 29){
				  for (var i = 1; i < 30; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 30; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		  }else{
			  if(birthDay <= 28){
				  for (var i = 1; i < 29; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 29; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		  }
		  
		}else if(birthMonth == 1 || birthMonth == 3 || birthMonth == 5 || birthMonth == 7 || birthMonth == 8 || birthMonth == 10 || birthMonth == 12){
			  if(birthDay <= 31){
				  for (var i = 1; i < 32; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 32; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		}else{
			if(birthDay <= 30){
				  for (var i = 1; i < 31; i++) {
					if(birthDay == i){
						selected = 'selected';
					}else{
						selected = '';
					}
					options += '<option value="' + i + '"'+ selected +' >' + i + '</option>';
				  }
			  }else{
					 for (var i = 1; i < 31; i++) {
						options += '<option value="' + i + '">' + i + '</option>';
					 }
			  }
		}
		$("select#birthday_day").html(options);
	});
});
function checkleapyear(datea){
	var datea = parseInt(datea);

	if(datea%4 == 0){
		if(datea%100 != 0){
			return true;
		}else{
			if(datea%400 == 0)
				return true;
			else
				return false;
		}
	}
	return false;
}


