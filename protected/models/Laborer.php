<?php

/**
 * This is the model class for table "laborer".
 *
 * The followings are the available columns in table 'laborer':
 * @property integer $id
 * @property string $name
 * @property string $date_of_birth
 * @property string $place_of_birth
 * @property integer $id_number
 * @property string $job_position
 * @property string $date_of_hiring
 * @property string $kin_of_contract
 * @property string $contact_expiring_date
 * @property string $place_of_work
 * @property string $tco_badge_number
 * @property string $sicim_badge_number
 */
class Laborer extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'laborer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, date_of_birth, place_of_birth, id_number, job_position, date_of_hiring, kin_of_contract, contact_expiring_date, place_of_work, tco_badge_number, sicim_badge_number', 'required'),
			array('id_number', 'numerical', 'integerOnly'=>true),
			array('name, place_of_birth, job_position', 'length', 'max'=>40),
			array('kin_of_contract, place_of_work, tco_badge_number, sicim_badge_number', 'length', 'max'=>20),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('id, name, date_of_birth, place_of_birth, id_number, job_position, date_of_hiring, kin_of_contract, contact_expiring_date, place_of_work, tco_badge_number, sicim_badge_number', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'date_of_birth' => 'Date Of Birth',
			'place_of_birth' => 'Place Of Birth',
			'id_number' => 'Id Number',
			'job_position' => 'Job Position',
			'date_of_hiring' => 'Date Of Hiring',
			'kin_of_contract' => 'Kin Of Contract',
			'contact_expiring_date' => 'Contact Expiring Date',
			'place_of_work' => 'Place Of Work',
			'tco_badge_number' => 'Tco Badge Number',
			'sicim_badge_number' => 'Sicim Badge Number',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('date_of_birth',$this->date_of_birth,true);
		$criteria->compare('place_of_birth',$this->place_of_birth,true);
		$criteria->compare('id_number',$this->id_number);
		$criteria->compare('job_position',$this->job_position,true);
		$criteria->compare('date_of_hiring',$this->date_of_hiring,true);
		$criteria->compare('kin_of_contract',$this->kin_of_contract,true);
		$criteria->compare('contact_expiring_date',$this->contact_expiring_date,true);
		$criteria->compare('place_of_work',$this->place_of_work,true);
		$criteria->compare('tco_badge_number',$this->tco_badge_number,true);
		$criteria->compare('sicim_badge_number',$this->sicim_badge_number,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Laborer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
}
