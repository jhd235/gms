<?php
/* @var $this ManhoursController */
/* @var $model Manhours */

$this->breadcrumbs=array(
	'Manhours'=>array('index'),
	$model->name=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List Manhours', 'url'=>array('index')),
	array('label'=>'Create Manhours', 'url'=>array('create')),
	array('label'=>'View Manhours', 'url'=>array('view', 'id'=>$model->id)),
	array('label'=>'Manage Manhours', 'url'=>array('admin')),
);
?>

<h1>Update Manhours <?php echo $model->id; ?></h1>

<?php $this->renderPartial('_form', array('model'=>$model)); ?>