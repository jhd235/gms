<?php
	define('INCLUDE_CHECK',1);
	require "functions.php";
?>
<!DOCTYPE html>
<html lang="en" id="facebook" class="no_js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<title>Register</title>
	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.2/jquery.min.js"></script>
	<script type="text/javascript" src="js/script.js"></script>
	<link type="text/css" rel="stylesheet" href="css/q39qoXKShfX.css" />
    <link type="text/css" rel="stylesheet" href="css/Db4ZSbwMr43.css" />
	
<body class="fbIndex">
		<div class="gradient">
			<div class="gradientContent">
				<div class="clearfix fbIndexFeaturedRegistration">
					<div class="signupForm lfloat">
						<div class="mbm phm headerTextContainer">
							<div class="mbs mainTitle fsl fwb fcb">Register</div>
							<div class="mbm subtitle fsm fwn fcg">SICIM S.p.A.</div>
						</div>
						<div id="registration_container">
							<div data-referrer="simple_registration_form">
							<noscript><div id="no_js_box"><h2>Javascript is disabled on your browser.</h2><p>Please enable Javascript on your browser or upgrade to a Javascript-capable browser to register for Facebook.</p></div></noscript>
							<div id="simple_registration_container" class="simple_registration_container">
							<div id="reg_box">
								<form method="post" id="reg" name="reg" >
									<div id="reg_form_box" class="large_form">
										<table class="uiGrid editor" cellspacing="0" cellpadding="1">
											<tbody>
												<tr>
													<td class="label">
														<label for="firstname">First Name:</label>
													</td>
													<td>
														<div class="field_container">
															<input type="text" class="inputtext" id="firstname" name="firstname" />													   </div>
													</td>
												</tr>
												<tr>
													<td class="label">
														<label for="lastname">Last Name:</label>
													</td>
													<td>
															<div class="field_container">
																<input type="text" class="inputtext" id="lastname" name="lastname" />
															</div>
													</td>
												</tr>
												<tr>
													<td class="label">
														<label for="reg_email__">Your email address:</label>
													</td>
													<td>
														<div class="field_container">
															<input type="text" class="inputtext" id="reg_email__" name="reg_email__" autocomplete="off" />
														</div>
													</td>
												</tr>
												<tr>
													<td class="label">
														<label for="reg_email_confirmation__">Reenter email address:</label>
													</td>
													<td>
														<div class="field_container">
															<input type="text" class="inputtext" id="reg_email_confirmation__" name="reg_email_confirmation__" autocomplete="off" />
														</div>
													</td>
												</tr>
												<tr>
													<td class="label">
														<label for="reg_passwd__">New Password:</label>
													</td>
													<td>
														<div class="field_container">
														<input type="password" class="inputtext" id="reg_passwd__" name="reg_passwd__" value="" autocomplete="off" />
														</div>
													</td>
												</tr>
												<tr>
													<td class="label">I am:</td>
													<td>
														<div class="field_container">
															<select class="select" name="sex" id="sex">
																<option value="0">Select Gender:</option>
																<option value="1">Female</option>
																<option value="2">Male</option>
															</select>
														</div>
													</td>
												</tr>
												<tr>
													<td class="label">Birthday:</td>
													<td>
														<div class="field_container">
															<select name="birthday_day" id="birthday_day" class="">
																<option value="0">Day:</option>
																<?php echo generate_options(1,31); ?>
															</select>
															<select name="birthday_month" id="birthday_month" class="">
																<option value="0">Month:</option>
																<?php echo generate_options(1,12,'callback_month'); ?>
															</select>
															<select name="birthday_year" id="birthday_year" class="" >
																<option value="0">Year:</option>
																<?php echo generate_options(date('Y'),1900); ?>
															</select>
														</div>
													</td>
												</tr>
												<tr>
													<td class="label"></td>
													<td>
													<div id="birthday_warning">
														<a href="" title="Click for more information" rel="dialog">Why do I need to provide my date of birth?</a>
														</div>
													</td>
												</tr>
											</tbody>
										</table>
										<div class="reg_btn clearfix">
											<label class="uiButton uiButtonSpecial" for="u89aj1_3"><input value="Register" type="submit" id="u89aj1_3" /></label>
											<span id="async_status" class="async_status" style="display:none;"><img class="img" src="https://s-static.ak.facebook.com/rsrc.php/v1/yb/r/GsNJNwuI-UM.gif" alt="" width="16" height="11"></span>
										</div>
									</div>
								</form>
								<div id="reg_error" class="hidden_elem" style="height: auto; opacity:0">
									<div id="reg_error_inner"></div>
								</div>
								
							</div>
						</div>
					</div>
				</div>
			</div>
			
		</div>
		<div style="float:left;"><script type="text/javascript"><!--
google_ad_client = "ca-pub-4402126692151852";
/* Leaderboard */
google_ad_slot = "4940417807";
google_ad_width = 728;
google_ad_height = 90;
//-->
</script>
<!--<script type="text/javascript"
src="http://pagead2.googlesyndication.com/pagead/show_ads.js">
</script>--></div>
	</div>
</div>
</body>
</html>